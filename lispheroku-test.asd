(defsystem lispheroku-test
  :author " <>"
  :license ""
  :description "Tests for lispheroku."
  :depends-on (:lispheroku
               :fiveam)
  :components ((:module "t"
                :serial t
                :components
                ((:file "lispheroku")))))
