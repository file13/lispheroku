;;(require :lispheroku)
;;(ql:quickload :lispheroku)
;;(in-package :lispheroku)
(in-package #:cl-user)

(defmethod lucerne.ctl:start ((app lucerne.app:base-app) &key (port 8000) (server :hunchentoot) (address "127.0.0.1") debug silent)
  "Bring up @cl:param(app), by default on @cl:param(port) 8000. If the server
was not running, it returns @c(T). If the server was running, it restarts it and
returns @c(NIL)."
  (let ((rebooted nil))
    (when (lucerne.app:handler app)
      ;; The handler already exists, meaning the server is running. Bring it
      ;; down before bringing it up again.
      (setf rebooted t)
      (clack:stop (lucerne.app:handler app)))
    (setf (lucerne.app:handler app)
          (clack:clackup
           (lack:builder (let ((clack-app (lucerne.app:build-app app)))
                           (if debug
                               (funcall clack-errors:*clack-error-middleware*
                                        clack-app
                                        :debug t)
                               clack-app)))
           :port port
           :server server
           :address address
           :use-default-middlewares nil
           :silent silent))
    (sleep 1)
    ;; If it was rebooted, return nil. Otherwise t.
    (not rebooted)))

(defun initialize-application (&key port)
  (format t "In initialize-application with lucerne...~%")
  (lucerne:start lispheroku:app :port port :debug t)

  #|
  (defvar *handler*
    (clack:clackup
      (lambda (env)
        (declare (ignore env))
        '(200 (:content-type "text/plain") ("Hello, Clack!")))
      :server :hunchentoot
      :address "0.0.0.0"
      :port port))
  |#

  (format t "After running lucerne:start...~%"))

#|

;; old default code

(hunchentoot:define-easy-handler (root :uri "/") ()
  (cl-who:with-html-output-to-string (s nil :prologue t)
    (:html
      (:body
        (:p "hello, world")
        (:img :src "lisp-logo120x80.png")))))

(defvar *acceptor* nil)


(defun initialize-application (&key port)
  (setf hunchentoot:*dispatch-table*
    `(hunchentoot:dispatch-easy-handlers
       ,(hunchentoot:create-folder-dispatcher-and-handler
          "/" "/app/static/")))

  (when *acceptor*
    (hunchentoot:stop *acceptor*))

  (setf *acceptor*
        (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port port))))

|#
