(in-package :cl-user)
(defpackage lispheroku
  (:use :cl :lucerne)
  (:export :app)
  (:documentation "Main lispheroku code."))
(in-package :lispheroku)
(annot:enable-annot-syntax)

;;; App

(defapp app
  :middlewares ((clack.middleware.static:<clack-middleware-static>
                 :root (asdf:system-relative-pathname :lispheroku #p"assets/")
                 :path "/static/")))

;;; Templates

(djula:add-template-directory
 (asdf:system-relative-pathname :lispheroku #p"templates/"))

(defparameter +index+ (djula:compile-template* "index.html"))

;;; Views

@route app "/"
(defview index ()
  (render-template (+index+)))
