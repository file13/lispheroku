(defsystem lispheroku
  :author " <>"
  :maintainer " <>"
  :license ""
  :version "0.1"

  :depends-on (:hunchentoot :clack :clack-handler-hunchentoot :lucerne :cl-who)
  :components (
               (:module "assets"
                        :components
                        ((:module "css"
                                  :components
                                  ((:static-file "style.css")))
                         (:module "js"
                                  :components
                                  ((:static-file "scripts.js")))))
               (:module "src"
                        :serial t
                        :components
                        ((:file "lispheroku")))
               (:module "."
                        :serial t
                        :components
                        ((:file "application")))
               ;;(:file "package")
               (:file "application")
               ) ; end componets
  :description ""
  :long-description
  #.(uiop:read-file-string
     (uiop:subpathname *load-pathname* "README.md"))
  :in-order-to ((test-op (test-op lispheroku-test))))
