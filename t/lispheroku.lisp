(in-package :cl-user)
(defpackage lispheroku-test
  (:use :cl :fiveam))
(in-package :lispheroku-test)

(def-suite tests
  :description "lispheroku tests.")
(in-suite tests)

(test simple-test
  (is
   (equal 1 1))
  (is-true
   (and t t)))

(run! 'tests)
